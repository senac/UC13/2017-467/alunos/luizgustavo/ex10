/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Aluno;
import br.com.senac.Notas;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gustavo
 */
public class Teste {

    public Teste() {
    }
   @Test
    public void maiorQue7(){
        Aluno aluno1 = new Aluno("Marcos", 10, 5);
        Aluno aluno2 = new Aluno("Felipe", 7, 7);
        Aluno aluno3 = new Aluno("Luiz", 2, 3);
        Aluno aluno4 = new Aluno("Gustavo", 8, 7);
            
            List<Aluno>list = new ArrayList<>();
            list.add(aluno1);
            list.add(aluno2);
            list.add(aluno3);
            list.add(aluno4);
            
            Notas notas = new Notas();
            double result = notas.NotasAlunos(list);
            assertEquals(3, result, 0.01);
            
    }
    
    
    
   
}
