
package br.com.senac;

public class Aluno {
    private String aluno;
    private double notaA;
    private double notaB;
    
    public Aluno() {
    }

    public Aluno(String aluno, double notaA, double notaB) {
        this.aluno = aluno;
        this.notaA = notaA;
        this.notaB = notaB;
        
        
    }

    public String getAluno() {
        return aluno;
    }

    public void setAluno(String aluno) {
        this.aluno = aluno;
    }

    public double getNotaA() {
        return notaA;
    }

    public void setNotaA(double notaA) {
        this.notaA = notaA;
    }

    public double getNotaB() {
        return notaB;
    }

    public void setNotaB(double notaB) {
        this.notaB = notaB;
    }
    
    
 

       
    
    
    
    
}
